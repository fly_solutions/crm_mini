class EmployeeDepartmentsController < ApplicationController
	before_filter :find_employee_department, only: [:show, :edit, :update, :destroy]

  def index
  	@employee_departments = EmployeeDepartment.all
  end

  def new
  	@employee_department = EmployeeDepartment.new
  end

  def create
  	@employee_department = EmployeeDepartment.new(employee_department_params)
  	@employee_departments = EmployeeDepartment.all
  	render action: 'new' and return unless @employee_department.save
  end

  def edit
  end

  def update
  	@employee_departments = EmployeeDepartment.all
  	render action: 'edit' and return unless @employee_department.update(employee_department_params)
  end

  def destroy
  	@employee_department.destroy
  end

  private
  def find_employee_department
  	@employee_department = EmployeeDepartment.find(params[:id])
  end

  def employee_department_params
  	params.require(:employee_department).permit(:id, :name, :active)
  end
end
