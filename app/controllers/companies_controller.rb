class CompaniesController < ApplicationController
	before_filter :find_company, only: [:show, :edit, :update, :destroy]

  def index
  	@companies = Company.all
  end

  def new
  	@company = Company.new
  end

  def create
  	@company = Company.new(company_params)
  	@companies = Company.all
  	render action: 'new' and return unless @company.save
  end

  def edit
  end

  def update
    @companies = Company.all
  	render action: 'edit' and return unless @company.update(company_params)
  end

  def destroy
    @company.destroy
  end

  private
  def find_company
  	@company = Company.find(params[:id])
  end

  def company_params
  	params.require(:company).permit(:id, :name, :address, :website, :email, :phone, :mobile, :fax)
  end
end
