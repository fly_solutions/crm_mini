class EmployeesController < ApplicationController
  before_filter :find_employee, only: [:show, :edit, :update, :destroy]

  def index
  	@employees = Employee.all
  end

  def new
    @employee_department = EmployeeDepartment.find(params[:employee_department_id])
  	@employee = Employee.new
  end

  def create
  	@employee = Employee.new(employee_params)
  	@employees = Employee.all
  	render action: 'new' and return unless @employee.save
  end

  def edit
    @employee_department = EmployeeDepartment.find(params[:employee_department_id])
  end

  def update
  	@employees = Employee.all
  	render action: 'edit' and return unless @employee.update(employee_params)
  end

  def destroy
  	@employee.destroy
  end

  private
  def find_employee
  	@employee = Employee.find(params[:id])
  end

  def employee_params
  	params.require(:employee).permit(:id, :first_name, :last_name, :address_line_1, :email, :phone, :mobile, :employee_department_id)
  end
end
