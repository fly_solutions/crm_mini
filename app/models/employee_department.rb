class EmployeeDepartment < ApplicationRecord
	acts_as_paranoid
	
	has_many :employees

	validates :name, presence: true
end
