class Company < ApplicationRecord
	acts_as_paranoid
	
	validates :name, presence: true
	validates :email, presence: true
end
