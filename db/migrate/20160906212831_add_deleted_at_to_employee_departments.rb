class AddDeletedAtToEmployeeDepartments < ActiveRecord::Migration[5.0]
  def change
    add_column :employee_departments, :deleted_at, :datetime
    add_index :employee_departments, :deleted_at
  end
end
