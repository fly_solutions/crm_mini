class ChangeDepartmentIdOnEmployees < ActiveRecord::Migration[5.0]
  def change
  	rename_column :employees, :department_id, :employee_department_id
  end
end
