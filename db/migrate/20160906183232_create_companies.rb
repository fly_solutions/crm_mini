class CreateCompanies < ActiveRecord::Migration[5.0]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :address
      t.string :vat_number
      t.string :website
      t.string :email
      t.string :phone
      t.string :mobile
      t.string :fax

      t.timestamps
    end
  end
end
