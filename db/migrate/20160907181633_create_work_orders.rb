class CreateWorkOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :work_orders do |t|
      t.datetime :start_date
      t.datetime :end_date
      t.boolean :whole_day_task
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
