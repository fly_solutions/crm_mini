class CreateEmployees < ActiveRecord::Migration[5.0]
  def change
    create_table :employees do |t|
      t.string :first_name
      t.string :last_name
      t.string :address_line_1
      t.string :email
      t.string :phone
      t.string :mobile
      t.integer :department_id

      t.timestamps
    end
  end
end
