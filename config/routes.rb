Rails.application.routes.draw do

  get 'settings/', to: "settings#index", as: "settings"

 

  devise_for :users

  resources :companies

  resources :employee_departments do
  	resources :employees
  end

  resources :work_orders

  root 'home#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
